//
//  ShopplingListMapVC.m
//  MapSDKDemo
//
//  Created by Minxin Guo on 12/11/14.
//  Copyright (c) 2014 aisle411 Inc. All rights reserved.
//

#import "ShopplingListMapVC.h"
#import "ListTableVC.h"

@interface ShopplingListMapVC ()<ProductCalloutOverlayDelegate>{
    NSMutableArray* jsonArray;//To store returned JSON
    MapBundle* mapBundle;
    MapController* mapController;
    AMDGetRequester* getRequester;
    NSString* path;
    UIActivityIndicatorView *indicator;
    UIView *indicatorBackground;
    NSDictionary* resultDict;
    NSDictionary *searchedResults;
    ProductCalloutOverlay* productCalloutOverlay;
    NSMutableArray *productSectionArray;    
}
@end

@implementation ShopplingListMapVC

- (void)viewDidLoad {
    [super viewDidLoad];
    getRequester = [[AMDGetRequester alloc] init];
    
    //Setup map area
    mapController = [[MapController alloc] init];
    mapController.view.frame = _mapView.frame;
    
    [self getMapFromServer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)showMap{
    // To parse the fetched map data
    MapBundleParser *parser = [[MapBundleParser alloc] initWithPathToArchive:path];
    mapBundle = [parser parse];
    
    //initialize the mapController, set its view to desired view
    mapController = [[MapController alloc] init];
    mapController.mapBundle = mapBundle;
    mapController.view.frame = _mapView.bounds;
    [_mapView addSubview:mapController.view];
    
    [mapController setFloor:1];
    [mapController compassAction:nil];
    [mapController setCompassEnabled:NO];
    mapController.view.backgroundColor = [UIColor whiteColor];
    [mapController setLogoBottomOffset:0];
    [self shoppinglistResultFromServer];//////
}

- (void)getMapFromServer{
    [self addIndicatorView];
    NSArray* toPassIn = [NSArray arrayWithObject:[NSString stringWithFormat:@"retailer_store_id=%@", _storeID]];
    
    // Use the getRequester to get the request URL
    NSString* url = [getRequester SendGetRequest:@"map" withParameters:toPassIn];
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        //get the last-modified time stamp from server
        NSString *server_timestamp;
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
        NSHTTPURLResponse *response;
        [NSURLConnection sendSynchronousRequest: request returningResponse: &response error: nil];
        if ([response respondsToSelector:@selector(allHeaderFields)]) {
            NSDictionary *dictionary = [response allHeaderFields];
            server_timestamp=[dictionary objectForKey:@"Last-Modified"];
            NSLog(@"server timestamp is %@",server_timestamp);
        }
        
        //check the map timestamp locally
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *time_stamp = [defaults objectForKey:_storeID];
        NSLog(@"user_defalut timestamp is %@", time_stamp);
        
        //if timestamp doesn't exist or is not equal to the server
        if(![server_timestamp isEqualToString:time_stamp]){
            
            //update timestamp locally
            [defaults setObject:server_timestamp forKey:_storeID];
            [defaults synchronize];
            NSLog(@"now user_defalut timestamp is %@",[defaults objectForKey:_storeID]);
            
            // Background processing
            NSData* urlData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
            if (urlData == nil) {
                UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No Data Was Received" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [errorAlert show];
                return;
            }
            
            // Update the UI/send notifications based on the
            // Results of the background processing
            dispatch_async( dispatch_get_main_queue(), ^{
                [self removeIndicatorView];
                if ([NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil] != nil) {
                    NSLog(@"JSON DETECTED");
                    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Map Not Found" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [errorAlert show];
                    for (UIView *subview in [_mapView subviews]) {
                        [subview removeFromSuperview];
                    }
                    return;
                }
                
                for (UIView *subview in [_mapView subviews]) {
                    [subview removeFromSuperview];
                }
                
                if (urlData) {
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    NSString  *documentsDirectory = [paths objectAtIndex:0];
                    
                    NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, [NSString stringWithFormat:@"%@.imap", _storeID]];
                    [urlData writeToFile:filePath atomically:YES];
                    
                    NSFileManager* fileMgr = [NSFileManager defaultManager];
                    path = filePath;
                    NSLog(@"new path is %@",path);
                    if ([fileMgr fileExistsAtPath:filePath]) {
                        NSLog(@"store map locally!");
                        [self showMap];
                    }
                    
                }else{
                    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No Data Was Received" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [errorAlert show];
                }
            });
        }
        else{
            //use cached map
            dispatch_async( dispatch_get_main_queue(), ^{
                [self removeIndicatorView];
                
                for (UIView *subview in [_mapView subviews]) {
                    [subview removeFromSuperview];
                }
                
                NSArray *exist_paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString  *exist_documentsDirectory = [exist_paths objectAtIndex:0];
                
                NSString* exist_filePath = [exist_documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.imap", _storeID] ];
                
                NSFileManager* fileMgr = [NSFileManager defaultManager];
                
                path = exist_filePath;
                NSLog(@"the exist path is %@",path);
                if ([fileMgr fileExistsAtPath:exist_filePath]) {
                    NSLog(@"found map!");
                    [self showMap];
                }else{
                    UIAlertView *alert = [[UIAlertView alloc]
                                          initWithTitle:@"The map is missing!" message:nil delegate:nil
                                          cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alert show];
                    [defaults setObject:@"" forKey:_storeID];
                    [defaults synchronize];
                }
            });
        }
    });
}

#pragma mark - get shopping list search results
- (void)shoppinglistResultFromServer{
    NSString* userInfoPath = [[NSBundle mainBundle] pathForResource:@"UserInfo" ofType:@"plist"];
    NSDictionary* userInfoDict = [[NSDictionary alloc] initWithContentsOfFile:userInfoPath];
    
    //create user search term dictionary
    NSArray* userInputArray = [_searchTerms componentsSeparatedByString:@","];
    NSMutableArray *items = [[NSMutableArray alloc] init];
    NSMutableDictionary *item;
    for(NSString *term in userInputArray)
    {
        item = [[NSMutableDictionary alloc] init];
        [item setValue:term forKey:@"name"];
        //[item setValue:@"1" forKey:@"quantity"];
        [items addObject:item];
    }
    
    NSMutableDictionary *userSearchTermDict = [[NSMutableDictionary alloc] init];
    [userSearchTermDict setValue:@"demoList" forKey:@"name"];
    [userSearchTermDict setValue:items forKey:@"items"];
    
    //set parameters for the post method
    NSMutableDictionary *arguments = [[NSMutableDictionary alloc] init];
    [arguments setValue:userInfoDict[@"partnerIdentifier"] forKey:@"partner_id"];
    [arguments setValue:_storeID forKey:@"retailer_store_id"];
    [arguments setValue:userSearchTermDict forKey:@"shopping_list_data"];
    [arguments setValue:[getRequester deviceToken] forKey:@"device_token"];
    
    [getRequester sendWithPostMethodAndCompletionHandler:arguments block:^(NSDictionary *resultDictionary){
        searchedResults=resultDictionary;
        [self createProductPins];
    }];
}

- (void)createProductPins{
    NSLog(@"%@",searchedResults);
    NSArray *resultArray= [searchedResults objectForKey:@"items"];
    productSectionArray = [[NSMutableArray alloc] init];
    NSMutableArray *productsArray = [[NSMutableArray alloc] init];
    
    for (NSDictionary *temp in resultArray) {
        FMProduct *prod = [[FMProduct alloc] init];
        if(temp[@"synonym_nm"]){
            prod.name = [temp objectForKey:@"synonym_nm"];
            prod.idn = [[temp objectForKey:@"synonym_id"] intValue];
        } else {
            prod.name = temp[@"name"];
        }
        
        NSArray* tempArray=temp[@"sections"];
        NSDictionary* actualItemDict=tempArray[0];
        
        FMSection *sec = [[FMSection alloc] init];
        sec.sublocation = [actualItemDict[@"item_sub_location_id"] intValue];
        sec.location = [actualItemDict[@"item_location_id"] intValue];
        sec.maplocation = [actualItemDict[@"map_location_id"] intValue];
        sec.aisleTitle =actualItemDict[@"aisle"];
        sec.title = actualItemDict[@"section"];
        
        [productSectionArray addObject:sec];
        
        prod.sections = [[NSMutableArray alloc] initWithArray:productSectionArray];
        [productsArray addObject:prod];

        [productSectionArray removeAllObjects];
    }
    
    //add overlay to map
    productCalloutOverlay = [[ProductCalloutOverlay alloc] init];
    productCalloutOverlay.products = productsArray;
    
    //set up callout delegate in order to display pin title
    productCalloutOverlay.productCalloutDelegate = self;
    
    // Add the overlay to the mapController
    [mapController addOverlay:productCalloutOverlay];

}

#pragma mark - product detail from chevron button
- (void) chevronClickedForItem: (OverlayItem *)item {
    //leaves for future use
    NSLog(@"in chevron button");
}

#pragma mark - ProductCalloutOverlayDelegate methods
//set title for the overlay, may modify this to satisfy actually needed display
- (NSString*)titleForItem:(OverlayItem *)item{
    FMSection* section = nil;
    if ([item isKindOfClass:[ProductOverlayItem class]]) {
        ProductOverlayItem* prItem = (ProductOverlayItem*)item;
        for (int i = 0; i < [prItem.products count]; ++i) {
            FMProduct* shlItem = [self productAtIndex:i forItem:item];
            if (shlItem != nil) {
                if (shlItem.sections != nil) {
                    section = [shlItem.sections objectAtIndex:0];
                    break;
                }
            }
        }
    }
    
    if (section != nil) {
        NSLog(@"%@",section.aisleTitle);
        return section.aisleTitle;
    } else {
        return @"Store";
    }
}

#pragma mark - to get products for an item
- (FMProduct*)productAtIndex:(NSUInteger)ind forItem:(OverlayItem *)item {
    FMProduct* shlItem = nil;
    
    if ([item isKindOfClass:[ProductOverlayItem class]]) {
        ProductOverlayItem* prItem = (ProductOverlayItem*)item;
        FMProduct* fpr = [prItem.products objectAtIndex:ind];
        
        NSArray *list = productSectionArray;
        for (shlItem in list) {
            if (shlItem.idn == fpr.idn) {
                break;
            }
        }
    }
    return shlItem;
}


#pragma mark - segue related
- (IBAction)listButton:(id)sender {
    [self performSegueWithIdentifier:@"itemList" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"itemList"]) {
        ListTableVC *tableView = [segue destinationViewController];
        tableView.listDict = searchedResults;
    }
}

#pragma mark - indicator view related

-(void)addIndicatorView{
    indicatorBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    indicatorBackground.center = self.view.center;
    indicatorBackground.layer.cornerRadius = 10;
    indicatorBackground.backgroundColor = [UIColor grayColor];
    
    indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    indicator.color = [UIColor blueColor];
    indicator.center = self.view.center;
    
    [indicator startAnimating];
    [self.view addSubview:indicatorBackground];
    [self.view addSubview:indicator];
}

-(void)removeIndicatorView{
    [indicatorBackground removeFromSuperview];
    [indicator removeFromSuperview];
}


@end
