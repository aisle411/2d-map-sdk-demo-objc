//
//  AMDStoreMapViewController.m
//  MapSDKDemo
//
//  Created by Tony Dong on 7/9/14.
//  Copyright (c) 2014 aisle411 Inc. All rights reserved.
//

#import "AMDStoreMapViewController.h"

#import "AMDGetRequester.h"

#import "MapBundle.h"
#import "MapController.h"
#import "MapBundleParser.h"
#import <MapKit/MapKit.h>

@interface AMDStoreMapViewController ()

@end

@implementation AMDStoreMapViewController{
    NSMutableArray* jsonArray;//To store returned JSON
    MapBundle* mapBundle;
    MapController* mapController;
    AMDGetRequester* getRequester;
    NSString* path;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    getRequester = [[AMDGetRequester alloc] init];
    
    //Setup map area
    mapController = [[MapController alloc] init];
    mapController.view.frame = _resultArea.frame;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showMap{
    // To parse the fetched map data
    MapBundleParser *parser = [[MapBundleParser alloc] initWithPathToArchive:path];
    mapBundle = [parser parse];
    
    //initialize the mapController, set its view to desired view
    mapController = [[MapController alloc] init];
    mapController.mapBundle = mapBundle;
    mapController.view.frame = _resultArea.bounds;
    [_resultArea addSubview:mapController.view];
    
    [mapController setFloor:1];
    [mapController compassAction:nil];
    [mapController setCompassEnabled:NO];
    mapController.view.backgroundColor = [UIColor whiteColor];
    [mapController setLogoBottomOffset:0];
}

- (IBAction)goButtonPressed:(id)sender {
    _messageArea.textColor = [UIColor blackColor];
    _messageArea.text = @"Loading...";
    
    //Hide Keyboard
    [_storeIdArea endEditing:YES];
    
    
    if ([_storeIdArea.text isEqualToString:@""]) {
        _messageArea.textColor = [UIColor redColor];
        _messageArea.text = @"Please enter a valid Store ID";
        return;
    }
    
    else{
        // The parameters for the getRequester
        NSArray* toPassIn = [NSArray arrayWithObject:[NSString stringWithFormat:@"retailer_store_id=%@", _storeIdArea.text]];
        
        // Use the getRequester to get the request URL
        NSString* url = [getRequester SendGetRequest:@"map" withParameters:toPassIn];
        
        dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            //get the last-modified time stamp from server
            NSString *server_timestamp;
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
            NSHTTPURLResponse *response;
            [NSURLConnection sendSynchronousRequest: request returningResponse: &response error: nil];
            if ([response respondsToSelector:@selector(allHeaderFields)]) {
                NSDictionary *dictionary = [response allHeaderFields];
                server_timestamp=[dictionary objectForKey:@"Last-Modified"];
                NSLog(@"server timestamp is %@",server_timestamp);
            }
            
            //check the map timestamp locally
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *time_stamp = [defaults objectForKey:_storeIdArea.text];
            NSLog(@"user_defalut timestamp is %@", time_stamp);
            
            //if timestamp doesn't exist or is not equal to the server
            if(![server_timestamp isEqualToString:time_stamp]){
                
                //update timestamp locally
                [defaults setObject:server_timestamp forKey:_storeIdArea.text];
                [defaults synchronize];
                NSLog(@"now user_defalut timestamp is %@",[defaults objectForKey:_storeIdArea.text]);
                
                // Background processing
                NSData* urlData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
                //NSLog(@"Data From Show Map Controller: %@", urlData);
                if (urlData == nil) {
                    _messageArea.textColor = [UIColor redColor];
                    _messageArea.text = @"No Data Was Received";
                    return;
                }
                
                // Update the UI/send notifications based on the
                // Results of the background processing
                dispatch_async( dispatch_get_main_queue(), ^{
                    if ([NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil] != nil) {
                        NSLog(@"JSON DETECTED");
                        _messageArea.textColor = [UIColor redColor];
                        _messageArea.text = @"Map Not Found";
                        for (UIView *subview in [_resultArea subviews]) {
                            [subview removeFromSuperview];
                        }
                        return;
                    }
                    
                    _messageArea.textColor = [UIColor blackColor];
                    _messageArea.text = @"";
                    for (UIView *subview in [_resultArea subviews]) {
                        [subview removeFromSuperview];
                    }
                    
                    if (urlData) {
                        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                        NSString  *documentsDirectory = [paths objectAtIndex:0];
                        
                        NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, [NSString stringWithFormat:@"%@.imap", _storeIdArea.text]];
                        [urlData writeToFile:filePath atomically:YES];
                        
                        NSFileManager* fileMgr = [NSFileManager defaultManager];
                        path = filePath;
                        NSLog(@"new path is %@",path);
                        if ([fileMgr fileExistsAtPath:filePath]) {
                            _messageArea.textColor = [UIColor redColor];
                            _messageArea.text = [NSString stringWithFormat:@"Cache Map for ID: %@", _storeIdArea.text];
                            NSLog(@"store map locally!");
                            [self showMap];
                        }
                        
                    }else{
                        _messageArea.textColor = [UIColor redColor];
                        _messageArea.text = @"There Was No Data Received";
                    }
                });
            }
            else{
                //use cached map
                dispatch_async( dispatch_get_main_queue(), ^{
                    
                    for (UIView *subview in [_resultArea subviews]) {
                        [subview removeFromSuperview];
                    }
                    
                    NSArray *exist_paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    NSString  *exist_documentsDirectory = [exist_paths objectAtIndex:0];
                    
                    NSString* exist_filePath = [exist_documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.imap", _storeIdArea.text] ];
                    
                    NSFileManager* fileMgr = [NSFileManager defaultManager];
                    
                    path = exist_filePath;
                    NSLog(@"the exist path is %@",path);
                    if ([fileMgr fileExistsAtPath:exist_filePath]) {
                        _messageArea.textColor = [UIColor greenColor];
                        _messageArea.text = [NSString stringWithFormat:@"Found Local Map for ID: %@", _storeIdArea.text];
                        NSLog(@"found map!");
                        [self showMap];
                    }else{
                        UIAlertView *alert = [[UIAlertView alloc]
                                              initWithTitle:@"The map is missing!" message:nil delegate:nil
                                              cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                        [alert show];
                        [defaults setObject:@"" forKey:_storeIdArea.text];
                        [defaults synchronize];
                    }
                });
            }
        });
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    if ([_storeIdArea isFirstResponder] && [touch view] != _storeIdArea) {
        [_storeIdArea resignFirstResponder];
    }
    [super touchesBegan:touches withEvent:event];
}
@end
