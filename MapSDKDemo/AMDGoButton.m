//
//  AMDGoButton.m
//  MapSDKDemo
//
//  Created by Tony Dong on 7/9/14.
//  Copyright (c) 2014 aisle411 Inc. All rights reserved.
//

#import "AMDGoButton.h"

@implementation AMDGoButton

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.layer.cornerRadius = 10;
        self.clipsToBounds = YES;
        self.backgroundColor = [UIColor colorWithRed:0.2 green:0.8 blue:0.2 alpha:1];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
