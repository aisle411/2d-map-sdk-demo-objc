//
//  AMDDetailedInfoViewController.h
//  MapSDKDemo
//
//  Created by Tony Dong on 7/11/14.
//  Copyright (c) 2014 aisle411 Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMDDetailedInfoViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *productNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *itemIDLabel;
@property (strong, nonatomic) IBOutlet UILabel *upcLabel;
@property (strong, nonatomic) IBOutlet UILabel *aisleLabel;

@property (strong, nonatomic) IBOutlet UIButton *doneButton;

@property NSString* productName;
@property NSInteger* itemID;
@property NSString* upc;
@property NSString* aisle;

@end
