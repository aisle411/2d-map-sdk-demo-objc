//
//  AMDCombinedViewController.m
//  MapSDKDemo
//
//  Created by Tony Dong on 7/9/14.
//  Copyright (c) 2014 aisle411 Inc. All rights reserved.
//

#import "AMDCombinedViewController.h"

#import "AMDGetRequester.h"

#import "MapBundle.h"
#import "MapController.h"
#import "MapBundleParser.h"
#import "FMSection.h"
#import "FMProduct.h"
#import "ProductCalloutOverlay.h"
#import "CalloutOptionOverlay.h"


@interface AMDCombinedViewController () <ProductCalloutOverlayDelegate>{
    NSArray* toUseSectionAry;
}

@end

@implementation AMDCombinedViewController{
    // Declare Necessary SDK objects
    MapBundle* mapBundle;
    MapController* mapController;
    ProductCalloutOverlay* productCalloutOverlay;
    AMDGetRequester* getRequester;
    
    // Declare Necessary storage variables
    NSString* storeID;
    NSMutableData* responseMapData;
    NSDictionary* resultDict;
    NSString* path;
    
    // To indicate wether user entered an UPC or a term
    BOOL usingUPC;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    getRequester = [[AMDGetRequester alloc] init];
    
    //Setup map area
    mapController = [[MapController alloc] init];
    mapController.view.frame = _resultArea.frame;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goButtonPressed:(id)sender {
    // Hide keyboards
    [_storeIdArea endEditing:YES];
    [_termArea endEditing:YES];
    
    // Display message if user input was empty
    if ([_termArea.text isEqualToString:@""] || [_storeIdArea.text isEqualToString:@""]) {
        _messageArea.textColor = [UIColor redColor];
        _messageArea.text = @"Please input both parameters";
        return;
    }
    
    _messageArea.textColor = [UIColor purpleColor];
    _messageArea.text = @"Loading Search Result...";
    
    
    /****** Following is for fetching search result ******/
    
    // To determine if user entered number(UPC) or string(term)
    NSScanner* scanner = [NSScanner scannerWithString:_termArea.text];
    NSInteger* integer = NULL;
    
    // The parameters for the getRequester
    NSArray* toPassIn;
    
    if ([scanner scanInteger:integer]) {
        toPassIn = [NSArray arrayWithObjects:[NSString stringWithFormat:@"retailer_store_id=%@", _storeIdArea.text], [NSString stringWithFormat:@"upc=%@", _termArea.text], @"start=0", @"end=7", nil];
        usingUPC = true;
    }else{
        toPassIn = [NSArray arrayWithObjects:[NSString stringWithFormat:@"retailer_store_id=%@", _storeIdArea.text], [NSString stringWithFormat:@"term=%@", _termArea.text], @"start=0", @"end=7", nil];
        usingUPC = false;
    }
    
    
    // Use the getRequester to get the request URL
    NSString* url = [getRequester SendGetRequest:@"searchproduct" withParameters:toPassIn];
    
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        // Background processing
        
        NSData* dataLoad = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:url]];
        
        if (dataLoad == nil) {
            _messageArea.textColor = [UIColor redColor];
            _messageArea.text = @"No Data Was Received";
            return;
        }
        
        dispatch_async( dispatch_get_main_queue(), ^{
            
            // Update the UI/send notifications based on the
            // Results of the background processing
            
            NSError* error;
            
            if (usingUPC) {
                resultDict = [NSJSONSerialization JSONObjectWithData:dataLoad options:NSJSONReadingMutableContainers error:&error];
            }else{
                resultDict = [NSJSONSerialization JSONObjectWithData:dataLoad options:NSJSONReadingMutableContainers error:&error];
            }
            
            
            if (error) {
                _messageArea.textColor = [UIColor redColor];
                _messageArea.text = @"Failed to Search";
            }else{
                _messageArea.textColor = [UIColor purpleColor];
                _messageArea.text = @"Success, Now Loading Map....";
            }
        });
    });
    
    
    
    
    /****** Following is for fetching map data ******/

    
    NSArray* toPassInForStoreMap = [NSArray arrayWithObject:[NSString stringWithFormat:@"retailer_store_id=%@", _storeIdArea.text]];
    storeID = _storeIdArea.text;
    
    NSString* urlForStoreMap = [getRequester SendGetRequest:@"map" withParameters:toPassInForStoreMap];
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        // Background processing
        
        NSData* urlDataForStoreMap = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlForStoreMap]];
        
        if (urlDataForStoreMap == nil) {
            _messageArea.textColor = [UIColor redColor];
            _messageArea.text = @"No Data Was Received";
            return;
        }
        
        dispatch_async( dispatch_get_main_queue(), ^{
            
            // Update the UI/send notifications based on the
            // Results of the background processing
            
            if ([NSJSONSerialization JSONObjectWithData:urlDataForStoreMap options:kNilOptions error:nil] != nil) {
                _messageArea.textColor = [UIColor redColor];
                _messageArea.text = @"Map Not Found";
                for (UIView* subview in [_resultArea subviews]) {
                    [subview removeFromSuperview];
                }
                return;
            }
            
            _messageArea.textColor = [UIColor blackColor];
            _messageArea.text = @"";
            for (UIView *subview in [_resultArea subviews]) {
                [subview removeFromSuperview];
            }
            
            if (urlForStoreMap) {
                NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString* documentsDirectory = [paths objectAtIndex:0];
                NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, [NSString stringWithFormat:@"%@.imap", _storeIdArea]];
                [urlDataForStoreMap writeToFile:filePath atomically:YES];
                
                NSFileManager* fileMgr = [NSFileManager defaultManager];
                
                path = filePath;
                
                if ([fileMgr fileExistsAtPath:filePath]) {
                    _messageArea.textColor = [UIColor greenColor];
                    _messageArea.text = [NSString stringWithFormat:@"Found Map for ID: %@", _storeIdArea.text];
                    [self showMap];
                    [self loadProductsOverlay];
                }
                
                
            }
        });
    });
}

- (void)showMap {
    
    // To parse the fetched map data
    MapBundleParser *parser = [[MapBundleParser alloc] initWithPathToArchive:path];
    mapBundle = [parser parse];
    
    //initialize the mapController, set its view to desired view
    mapController = [[MapController alloc] init];
    mapController.mapBundle = mapBundle;
    mapController.view.frame = _resultArea.bounds;
    [_resultArea addSubview:mapController.view];
    
    [mapController setFloor:1];
    [mapController compassAction:nil];
    [mapController setCompassEnabled:NO];
    mapController.view.backgroundColor = [UIColor whiteColor];
    [mapController setLogoBottomOffset:0];
}

/*********************
 Due to returned dictionary names are different
 Also have the same contents
 This method is fairly long to handle different cases in this demo
 *********************/

- (void)loadProductsOverlay{
    
    // To add sections to the product overlay
    NSMutableArray* sectionAry = [[NSMutableArray alloc] init];
    
    if (usingUPC) {
        if (resultDict[@"products"][@"item_nm"] != nil) {
            // If the result has item_nm value, it was successfully returned one product.
            
            for (int i = 0; i < [resultDict[@"products"][@"sections"] count]; i++) {
                FMSection* tempSec = [[FMSection alloc] initWithSublocation:(int)resultDict[@"products"][@"sections"][i][@"item_sub_location_id"] location:[resultDict[@"products"][@"sections"][i][@"item_location_id"] intValue]];
                tempSec.maplocation = [resultDict[@"products"][@"sections"][i][@"map_location_id"] intValue];
                tempSec.title = resultDict[@"products"][@"item_nm"];
                tempSec.aisleTitle = resultDict[@"products"][@"sections"][i][@"aisle"];
                
                FMProduct* tempPro = [[FMProduct alloc] init];
                tempPro.name = resultDict[@"products"][@"item_nm"];
                tempPro.sections = [NSArray arrayWithObject:tempSec];
                tempPro.idn = (int)resultDict[@"products"][@"item_id"];
                
                [sectionAry addObject:tempPro];
            }
            _messageArea.text = [NSString stringWithFormat:@"Found product By UPC"];
            
        }else{
            // If the result has no such value, it mean it returned empty result
            _messageArea.textColor = [UIColor redColor];
            _messageArea.text = [NSString stringWithFormat:@"Found Nothing By UPC"];
        }
    }else{
        if (resultDict[@"products"] != nil) {
            
            // If the returned dictionary has key products, it means found a exact match
            // Another chance is that the returned dictionary is empty (Found null)
            
            for (int i = 0; i < [resultDict[@"products"] count]; i++) {
                for (int j = 0; j < [resultDict[@"products"][i][@"sections"] count]; j++) {
                    FMSection* tempSec = [[FMSection alloc] initWithSublocation:(int)resultDict[@"products"][i][@"sections"][j][@"item_sub_location_id"] location:[resultDict[@"products"][i][@"sections"][j][@"item_location_id"] intValue]];
                    tempSec.maplocation = [resultDict[@"products"][i][@"sections"][j][@"map_location_id"] intValue];
                    tempSec.title = resultDict[@"products"][i][@"synonym_nm"];
                    tempSec.aisleTitle = resultDict[@"products"][i][@"sections"][j][@"aisle"];
                    
                    FMProduct* tempPro = [[FMProduct alloc] init];
                    tempPro.name = resultDict[@"products"][i][@"synonym_nm"];
                    tempPro.sections = [NSArray arrayWithObject:tempSec];
                    tempPro.idn = (int)resultDict[@"products"][i][@"synonym_id"];
                    
                    [sectionAry addObject:tempPro];
                }
            }
            
            _messageArea.text = [NSString stringWithFormat:@"Found Exact Match or Null"];
            
        }else if (resultDict[@"product_suggestions"] != nil){
            
            // If the returned dictionary has key products,
            //it means found a list of items that can be found by the term
            
            for (int i = 0; i < [resultDict[@"product_suggestions"] count]; i++) {
                for (int j = 0; j < [resultDict[@"product_suggestions"][i][@"sections"] count]; j++) {
                    FMSection* tempSec = [[FMSection alloc] initWithSublocation:(int)resultDict[@"product_suggestions"][i][@"sections"][j][@"item_sub_location_id"] location:[resultDict[@"product_suggestions"][i][@"sections"][j][@"item_location_id"] intValue]];
                    tempSec.maplocation = [resultDict[@"product_suggestions"][i][@"sections"][j][@"map_location_id"] intValue];
                    tempSec.title = resultDict[@"product_suggestions"][i][@"synonym_nm"];
                    tempSec.aisleTitle = resultDict[@"product_suggestions"][i][@"sections"][j][@"aisle"];
                    
                    FMProduct* tempPro = [[FMProduct alloc] init];
                    tempPro.name = resultDict[@"product_suggestions"][i][@"synonym_nm"];
                    tempPro.sections = [NSArray arrayWithObject:tempSec];
                    tempPro.idn = (int)resultDict[@"product_suggestions"][i][@"synonym_id"];
                    
                    [sectionAry addObject:tempPro];
                }
            }
            
            _messageArea.text = [NSString stringWithFormat:@"Found Multiple Suggestions"];
            
        }else{
            
            // Else, the server determines that the user entered a typo
            // Then the server will return a list of suggestion words
            // Returned dictionary have typo_suggestions key
            _messageArea.textColor = [UIColor orangeColor];
            _messageArea.text = @"Typo, returned suggestions";
        }
    }
    
    toUseSectionAry = [NSArray arrayWithArray:sectionAry];
    
    productCalloutOverlay = [[ProductCalloutOverlay alloc] init];
    
    productCalloutOverlay.products = toUseSectionAry;
    
    //set up callout delegate in order to display pin title
    productCalloutOverlay.productCalloutDelegate = self;
    
    // Add the overlay to the mapController
    [mapController addOverlay:productCalloutOverlay];
}

#pragma mark - product detail from chevron button
- (void) chevronClickedForItem: (OverlayItem *)item {
    
    //leaves for future use
    NSLog(@"in chevron button");
}

#pragma mark - ProductCalloutOverlayDelegate methods
//set title for the overlay, may modify this to satisfy actually needed display
- (NSString*)titleForItem:(OverlayItem *)item{
    FMSection* section = nil;
    if ([item isKindOfClass:[ProductOverlayItem class]]) {
        ProductOverlayItem* prItem = (ProductOverlayItem*)item;
        for (int i = 0; i < [prItem.products count]; ++i) {
            FMProduct* shlItem = [self productAtIndex:i forItem:item];
            if (shlItem != nil) {
                if (shlItem.sections != nil) {
                    section = [shlItem.sections objectAtIndex:0];
                    break;
                }
            }
        }
    }
    
    if (section != nil) {
        return section.aisleTitle;
    } else {
        return @"";
    }
}

#pragma mark - to get products for an item
- (FMProduct*)productAtIndex:(NSUInteger)ind forItem:(OverlayItem *)item {
    FMProduct* shlItem = nil;
    
    if ([item isKindOfClass:[ProductOverlayItem class]]) {
        ProductOverlayItem* prItem = (ProductOverlayItem*)item;
        FMProduct* fpr = [prItem.products objectAtIndex:ind];
        
        NSArray *list = toUseSectionAry;
        for (shlItem in list) {
            if (shlItem.idn == fpr.idn) {
                break;
            }
        }
    }
    return shlItem;
}

#pragma mark - keyboard resign
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    if ([_storeIdArea isFirstResponder] && [touch view] != _storeIdArea) {
        [_storeIdArea resignFirstResponder];
    }else{
        [_termArea resignFirstResponder];
    }
    [super touchesBegan:touches withEvent:event];
}
@end
