//
//  AMDProductListViewController.h
//  MapSDKDemo
//
//  Created by Tony Dong on 7/9/14.
//  Copyright (c) 2014 aisle411 Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMDProductListViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *textArea;

@property (strong, nonatomic) IBOutlet UIButton *goButton;

@property (strong, nonatomic) IBOutlet UILabel *messageArea;

@property (strong, nonatomic) IBOutlet UITableView *resultArea;
@property (strong, nonatomic) IBOutlet UITextField *termArea;
@property (strong, nonatomic) IBOutlet UITextField *storeIDtextArea;

@end
