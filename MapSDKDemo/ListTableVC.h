//
//  ListTableVC.h
//  MapSDKDemo
//
//  Created by Minxin Guo on 12/12/14.
//  Copyright (c) 2014 aisle411 Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListTableVC : UITableViewController
@property (nonatomic,weak) NSDictionary *listDict;

@end
