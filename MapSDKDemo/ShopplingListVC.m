//
//  ShopplingListVC.m
//  MapSDKDemo
//
//  Created by Minxin Guo on 12/11/14.
//  Copyright (c) 2014 aisle411 Inc. All rights reserved.
//

#import "ShopplingListVC.h"
#import "ShopplingListMapVC.h"

@interface ShopplingListVC () <UITextFieldDelegate>

@end

@implementation ShopplingListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _storeIDtextField.delegate = self;
    _searchTermTextField.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goButtonAction:(id)sender {
    
    if ([_storeIDtextField.text isEqualToString:@""]) {
        NSString *errorMsg = @"Please type in store ID";
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Oops" message:errorMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [errorAlert show];
    }
    
    if (![_storeIDtextField.text isEqualToString:@""]){
        [self performSegueWithIdentifier:@"pushToMap" sender:sender];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField.text.length==0) {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    if ([_storeIDtextField isFirstResponder] && [touch view] != _storeIDtextField) {
        [_storeIDtextField resignFirstResponder];
    }
    else if ([_searchTermTextField isFirstResponder] && [touch view] != _searchTermTextField) {
        [_searchTermTextField resignFirstResponder];
    }
    [super touchesBegan:touches withEvent:event];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"pushToMap"]) {
        ShopplingListMapVC *mapView = [segue destinationViewController];
        mapView.storeID = _storeIDtextField.text;
        mapView.searchTerms = _searchTermTextField.text;
    }
}

@end
