//
//  AMDProductListViewController.m
//  MapSDKDemo
//
//  Created by Tony Dong on 7/9/14.
//  Copyright (c) 2014 aisle411 Inc. All rights reserved.
//

#import "AMDProductListViewController.h"
#import "AMDGetRequester.h"
#import "AMDDetailedInfoViewController.h"

@interface AMDProductListViewController ()

@end

/*********************************
 In this Demo, Only GET requests to aisle411 server are used
 No header from MapSDK is necessary.
**********************************/

@implementation AMDProductListViewController{
    BOOL usingUPC;//To indicate if user entered an UPC or a term
    NSDictionary* resultDict;//To parse returned JSON into a NSDictionary
    AMDGetRequester* getRequester;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    getRequester = [[AMDGetRequester alloc] init];    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)goButtonPressed:(id)sender {
    // Hide Keyboard
    [_textArea endEditing:YES];
    [_storeIDtextArea endEditing:YES];
    
    // If User entered nothing, display a message instead
    //if ([_textArea.text isEqualToString:@""]) {
        
    if ([_textArea.text isEqualToString:@""]) {
        _messageArea.textColor = [UIColor redColor];
        _messageArea.text = @"Please Input a valid parameter";
        return;
    }
    
    else if ([_storeIDtextArea.text isEqualToString:@""]){
        _messageArea.textColor = [UIColor redColor];
        _messageArea.text = @"Please Type In StoreID";
        return;
    }
    
    else{
        // Indicates the search is in progress
        _messageArea.textColor = [UIColor blackColor];
        _messageArea.text = @"Please Wait...";
        
        // To determine wether the input is a number or a string
        NSScanner* scanner = [NSScanner scannerWithString:_textArea.text];
        NSInteger* integer = NULL;
        
        
        // The parameters for the getRequester
        NSArray* toPassIn;
        
        // To determine if user entered number(UPC) or string(term), retailer_store_id is fixed in this demo
        if ([scanner scanInteger:integer]) {
            toPassIn = [NSArray arrayWithObjects:[NSString stringWithFormat:@"retailer_store_id=%@", _storeIDtextArea.text], [NSString stringWithFormat:@"upc=%@", _textArea.text], @"start=0", @"end=7", nil];
            usingUPC = true;
        }else{
            toPassIn = [NSArray arrayWithObjects:[NSString stringWithFormat:@"retailer_store_id=%@", _storeIDtextArea.text], [NSString stringWithFormat:@"term=%@", _textArea.text], @"start=0", @"end=7", nil];
            usingUPC = false;
        }
        
        //Use the getRequester to get the request URL
        NSString* url = [getRequester SendGetRequest:@"searchproduct" withParameters:toPassIn];
        
        //Run the search in the background
        dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            // Background processing
            
            NSData* dataLoad = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:url]];
            
            if (dataLoad == nil) {
                _messageArea.textColor = [UIColor redColor];
                _messageArea.text = @"No Data Was Received";
                return;
            }
            
            dispatch_async( dispatch_get_main_queue(), ^{
                
                // Results of the background processing
                
                NSError* error;
                
                resultDict = [NSJSONSerialization JSONObjectWithData:dataLoad options:NSJSONReadingMutableContainers error:&error];
                
                
                if (error) {
                    _messageArea.textColor = [UIColor redColor];
                    _messageArea.text = @"There was an Error";
                    
                }else{
                    _messageArea.textColor = [UIColor greenColor];
                    
                    [self reloadList];
                    
                }
                
                [_resultArea reloadData];
            });
        });
    }
}


// Only for message displaying purposes, shows all possible cases.
-(void)reloadList{
    if (usingUPC) {
        if (resultDict[@"products"][@"item_nm"] != nil) {
            // If the result has item_nm value, it was successfully returned one product.
            
            _messageArea.text = [NSString stringWithFormat:@"Found product By UPC"];
            
        }else{
            // If the result has no such value, it mean it returned empty result
            
            _messageArea.textColor = [UIColor redColor];
            _messageArea.text = [NSString stringWithFormat:@"Found Nothing By UPC"];
            
        }
    }else{
        if (resultDict[@"products"] != nil) {
            
            // If the returned dictionary has key products, it means found a exact match
            // Another chance is that the returned dictionary is empty (Found null)
            
            _messageArea.text = [NSString stringWithFormat:@"Found Exact Match or Null"];
            
        }else if (resultDict[@"product_suggestions"] != nil){
            
            // If the returned dictionary has key products,
            //it means found a list of items that can be found by the term
            
            _messageArea.text = [NSString stringWithFormat:@"Found Multiple Suggestions"];
            
        }else{
            
            // Else, the server determines that the user entered a typo
            // Then the server will return a list of suggestion words
            // Returned dictionary have typo_suggestions key
            _messageArea.textColor = [UIColor orangeColor];
            _messageArea.text = @"Typo, returned suggestions";
            
        }
    }
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section{
    if (usingUPC) {
        
        // If using upc, there are only 0 or 1 possible returns
        
        return 1;
        
    }else{
        
        //Consider all possible returns.
        
        if (resultDict[@"products"] != nil) {
            return [resultDict[@"products"] count];
        }else if (resultDict[@"product_suggestions"] != nil){
            return [resultDict[@"product_suggestions"] count];
        }else{
            return [resultDict[@"typo_suggestions"] count];
        }
    }
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell* cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellID"];
    
    
    if (usingUPC) {
        if (resultDict[@"products"][@"item_nm"] == nil) {
            // If the retuened value is empty
            return cell;
        }
        cell.textLabel.text = resultDict[@"products"][@"item_nm"];
    }else{
        if (resultDict[@"products"] != nil) {
            cell.textLabel.text = resultDict[@"products"][indexPath.row][@"synonym_nm"];
        }else if (resultDict[@"product_suggestions"] != nil){
            cell.textLabel.text = resultDict[@"product_suggestions"][indexPath.row][@"synonym_nm"];
        }else{
            cell.textLabel.text = resultDict[@"typo_suggestions"][indexPath.row];
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AMDDetailedInfoViewController* info = [self.storyboard instantiateViewControllerWithIdentifier:@"infoVC"];
    
    if (usingUPC) {
        if (resultDict[@"products"][@"item_nm"] != nil) {
            info.productName = resultDict[@"products"][@"item_nm"];
            info.itemID = (int)resultDict[@"products"][@"item_id"];
            info.upc = resultDict[@"products"][@"upc"];
            info.aisle = resultDict[@"products"][@"sections"][0][@"aisle"];
        }
    }else{
        if (resultDict[@"products"] != nil) {
            info.productName = resultDict[@"products"][indexPath.row][@"synonym_nm"];
            info.itemID = (int)resultDict[@"products"][indexPath.row][@"synonym_id"];
            info.upc = @"unknown";
            info.aisle = resultDict[@"products"][indexPath.row][@"sections"][0][@"aisle"];
        }else if (resultDict[@"product_suggestions"] != nil){
            info.productName = resultDict[@"product_suggestions"][indexPath.row][@"synonym_nm"];
            info.itemID = (int)resultDict[@"product_suggestions"][indexPath.row][@"synonym_id"];
            info.upc = @"unknown";
            info.aisle = resultDict[@"product_suggestions"][indexPath.row][@"sections"][0][@"aisle"];
        }else{
            info.productName = @"aisle411 Typo Suggestion";
            info.itemID = nil;
            info.upc = resultDict[@"typo_suggestions"][indexPath.row];
            info.aisle = _textArea.text;
        }
    }
    
    [[info view] setNeedsDisplay];
    [self presentViewController:info animated:YES completion:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    if ([touch view] != _textArea) {
        [_textArea endEditing:YES];
    }
    [super touchesBegan:touches withEvent:event];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
