//
//  AMDGetRequester.h
//  MapSDKDemo
//
//  Created by Tony Dong on 7/10/14.
//  Copyright (c) 2014 aisle411 Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AMDGetRequester : NSObject{
    /****************************************
     The Following Information Must be Entered in the UserInfo.plist in Supporting Files
     ****************************************/
    NSString *partnerID;//partnerIndentifier in UserInfo.plist
    NSString *partnerSecret;//partnerSecret in UserInfo.plist
    NSString *baseURL;//Pre entered, testing server
}

-(NSString*)SendGetRequest:(NSString*)method withParameters:(NSArray*)parameters;
- (void)sendWithPostMethodAndCompletionHandler:	(NSMutableDictionary *)arguments block:(void(^)(NSDictionary *resultDictionary)) completeBlock;
-(NSString *)deviceToken;

@end
