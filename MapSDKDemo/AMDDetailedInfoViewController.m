//
//  AMDDetailedInfoViewController.m
//  MapSDKDemo
//
//  Created by Tony Dong on 7/11/14.
//  Copyright (c) 2014 aisle411 Inc. All rights reserved.
//

#import "AMDDetailedInfoViewController.h"

@interface AMDDetailedInfoViewController ()

@end

@implementation AMDDetailedInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _productNameLabel.text = [NSString stringWithFormat:@"Product Name: %@",_productName];
    _itemIDLabel.text = [NSString stringWithFormat:@"Item ID: %d", (int)_itemID];
    _upcLabel.text = [NSString stringWithFormat:@"UPC: %@",_upc];
    _aisleLabel.text = [NSString stringWithFormat:@"Aisle Label: %@",_aisle];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
