//
//  AMDGetRequester.m
//  MapSDKDemo
//
//  Created by Tony Dong on 7/10/14.
//  Copyright (c) 2014 aisle411 Inc. All rights reserved.
//

#import "AMDGetRequester.h"
#import <CommonCrypto/CommonDigest.h>
#import <UIKit/UIKit.h>

@implementation AMDGetRequester


/**************************************************
 Initialization is to find all User Information
 If user Information was not entered, it will throw exceptions in SendGetRequest Method
 *************************************************/
-(id) init{
    
    NSString* userInfoPath = [[NSBundle mainBundle] pathForResource:@"UserInfo" ofType:@"plist"];
    NSDictionary* userInfoDict = [[NSDictionary alloc] initWithContentsOfFile:userInfoPath];
    
    partnerID = userInfoDict[@"partnerIdentifier"];
    partnerSecret = userInfoDict[@"partnerSecret"];
    baseURL = userInfoDict[@"baseURL"];
    return self;
}

/*****************
 SendGetRequest will return an NSString containing the GET request URL
 *****************/
-(NSString*)SendGetRequest:(NSString*)method withParameters:(NSArray*)parameters{
    if ([partnerID length] == 0) {
        @throw [NSException exceptionWithName:@"Empty partner ID" reason:@"partnerIdentifier in UserInfo.plist must be specified" userInfo:nil];
    }
    if ([partnerSecret length] == 0) {
        @throw [NSException exceptionWithName:@"Empty partner Secret" reason:@"partnerSecret in UserInfo.plist must be specified" userInfo:nil];
    }
    NSString* url = [NSString stringWithFormat:@"%@/%@", baseURL, [self AddAuth:method withParameters:[parameters arrayByAddingObject:[NSString stringWithFormat:@"partner_id=%@", partnerID]]]];
    
    //Escape Spaces with '%20'
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"%@", url);
    
    return url;
}

/****************
 Using MD5 to add Authentication
 ***************/
-(NSString*)AddAuth:(NSString*)method withParameters:(NSArray*)parameters{
    
    //Step 1: Sort parameters
    NSArray* sortedResult = [parameters sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    //Step 2: Append sorted parameters one by one to the end of the request
    NSString* toHash = [method stringByAppendingString:@"?"];
    
    NSString* result = [method stringByAppendingString:@".php?"];
    
    for (int i = 0; i < [sortedResult count]; i++) {
        if (i > 0) {
            toHash = [toHash stringByAppendingString:@"&"];
            result = [result stringByAppendingString:@"&"];
        }
        toHash = [toHash stringByAppendingString:sortedResult[i]];
        result = [result stringByAppendingString:sortedResult[i]];
    }
    
    //Step 3: Add Partner Secret
    toHash = [toHash stringByAppendingString:[NSString stringWithFormat:@"&%@", partnerSecret]];
    
    
    //Step 4: Encrypt with MD5
    const char *cStr = [toHash UTF8String];
    
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    
    CC_MD5(cStr, strlen(cStr), digest);
    
    NSMutableString* hashed = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH *2];
    
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [hashed appendFormat:@"%02x", digest[i]];
    }
    
    //Step 5: Append generated authentication to the end of the request
    result = [[result stringByAppendingString:@"&auth="] stringByAppendingString:hashed];
    
    
    return result;
}

#pragma mark - device token related
static NSString *guid()
{
    if ([[NSUserDefaults standardUserDefaults] stringForKey:@"GUID"])
    {
        return [[NSUserDefaults standardUserDefaults] stringForKey:@"GUID"];
    }
    else
    {
        CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault);
        NSString *uuidString = (NSString *)CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, uuid));
        CFRelease(uuid);
        [[NSUserDefaults standardUserDefaults]
         setObject:uuidString forKey:@"GUID"];
        return uuidString;
    }
}

static NSString *sha1(NSString *string)
{
    NSData *dataToHash = [string dataUsingEncoding:NSUTF8StringEncoding];
    
    unsigned char hashBytes[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1([dataToHash bytes], (unsigned int)[dataToHash length], hashBytes);
    
    
    char hashString[2 * CC_SHA1_DIGEST_LENGTH + 1] = {0};
    int i = 0;
    for (i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
    {
        snprintf(&hashString[i * 2], 3, "%02x", hashBytes[i]);
    }
    
    return [NSString stringWithUTF8String:hashString];
}

-(NSString *)deviceToken
{
    return sha1(guid());
}

static NSString *md5(NSString* string)
{
    NSData *dataToHash = [string dataUsingEncoding:NSUTF8StringEncoding];
    
    unsigned char hashBytes[CC_MD5_DIGEST_LENGTH];
    CC_MD5([dataToHash bytes], (unsigned int)[dataToHash length], hashBytes);
    
    char hashString[2 * CC_MD5_DIGEST_LENGTH + 1] = {0};
    int i = 0;
    for (i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
    {
        snprintf(&hashString[i * 2], 3, "%02x", hashBytes[i]);
    }
    
    return [NSString stringWithUTF8String:hashString];
}

#pragma mark -post method
- (void)sendWithPostMethodAndCompletionHandler:	(NSMutableDictionary *)arguments block:(void(^)(NSDictionary *resultDictionary)) completeBlock
{
    NSString *url = @"https://aisle411.ws/webservices3/locateitems.php";
    
    //create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    
    //create data from pass-in arguments
    NSError *error = nil;
    NSData *data = [NSJSONSerialization dataWithJSONObject:arguments options:0 error:&error];
    
    //encoded body
    NSString *body = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    //remeber the method is post
    [request setHTTPMethod:@"POST"];
    
    //note here, it must use md5 for authentication
    //set http header field "Authentication" with json string + secret
    [request setValue:md5([body stringByAppendingString:partnerSecret]) forHTTPHeaderField:@"Authentication"];
    
    //set http body with json data
    [request setHTTPBody:data];

    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *responseCode, NSData *responseData, NSError *error) {
        
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&error];
        
        if (responseData == nil) {
            UIAlertView *emptyResultAlert=[[UIAlertView alloc] initWithTitle:@"Ooops.." message:@"No data was received." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [emptyResultAlert show];
            return;
        }
        else if (error) {
            UIAlertView *emptyResultAlert=[[UIAlertView alloc] initWithTitle:@"Ooops.." message:@"There was error processing the result." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [emptyResultAlert show];
        }
        else{
            if (completeBlock) completeBlock(resultDict);
        }
    }];
}

@end
