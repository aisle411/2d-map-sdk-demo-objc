//
//  AMDStoreMapViewController.h
//  MapSDKDemo
//
//  Created by Tony Dong on 7/9/14.
//  Copyright (c) 2014 aisle411 Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMDStoreMapViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *storeIdArea;

@property (strong, nonatomic) IBOutlet UIButton *goButton;

@property (strong, nonatomic) IBOutlet UIView *resultArea;
@property (strong, nonatomic) IBOutlet UILabel *messageArea;

@end
