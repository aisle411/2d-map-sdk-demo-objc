//
//  ShopplingListMapVC.h
//  MapSDKDemo
//
//  Created by Minxin Guo on 12/11/14.
//  Copyright (c) 2014 aisle411 Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapBundle.h"
#import "MapController.h"
#import "MapBundleParser.h"
#import "AMDGetRequester.h"
#import "FMSection.h"
#import "FMProduct.h"
#import "ProductCalloutOverlay.h"
#import "CalloutOptionOverlay.h"

@interface ShopplingListMapVC : UIViewController
@property (strong, nonatomic) IBOutlet UIView *mapView;
@property (strong, nonatomic) NSString *searchTerms;
@property (strong, nonatomic) NSString *storeID;

- (IBAction)listButton:(id)sender;


@end
