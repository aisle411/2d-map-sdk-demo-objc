//
//  ShopplingListVC.h
//  MapSDKDemo
//
//  Created by Minxin Guo on 12/11/14.
//  Copyright (c) 2014 aisle411 Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopplingListVC : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *searchTermTextField;
@property (strong, nonatomic) IBOutlet UITextField *storeIDtextField;
- (IBAction)goButtonAction:(id)sender;

@end
