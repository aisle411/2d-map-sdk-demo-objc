//
//  MapBundleParser.h
//  MapSDK
//
//  Created by Melkumova Lana on 7/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MapBundle.h"

@interface MapBundleParser : NSObject {
	NSString *_bundlePath;
	BOOL _deleteOnRecycle;
}

- (id) initWithPath:(NSString *)pathToBundle;
- (id) initWithPathToArchive:(NSString *)pathToArchive;

- (MapBundle *)parse;

@end
