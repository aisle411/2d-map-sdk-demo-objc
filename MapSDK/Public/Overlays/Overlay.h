/***********************************************************************
 *
 * aisle411
 * Copyright (C) by aisle411
 * http://www.aisle411.com
 *
 * Developed by Mercury Development, LLC
 * http://www.mercdev.com
 *
 ***********************************************************************/

#import <Foundation/Foundation.h>

/*!
 @class Overlay
 @abstract Base class for all overlays.
 */
@interface Overlay : NSObject {
	
}

@end
