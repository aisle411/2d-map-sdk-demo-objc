/***********************************************************************
 *
 * aisle411
 * Copyright (C) by aisle411
 * http://www.aisle411.com
 *
 * Developed by Mercury Development, LLC
 * http://www.mercdev.com
 *
 ***********************************************************************/

#import <Foundation/Foundation.h>

#import "Overlay.h"
#import "OverlayItem.h"
#import "CalloutOverlayDelegate.h"

@class CalloutView;
@class TiledView;
@class MapView;

/*!
 @class CalloutOverlay
 @abstract Callout overlay class definition.
 */
@interface CalloutOverlay : Overlay {
	NSMutableArray *items;
	OverlayItem *_selectedItem;
	
	NSMutableDictionary *_itemsPerFloor;
	CalloutView *_callout;
	
	UIImage *itemImage;
	UIImage *selectedItemImage;
	id<CalloutOverlayDelegate> calloutDelegate;
	
	BOOL showChevronInCallout;
    BOOL isCalloutFullyClickable;
}
/*!
 @property calloutDelegate
 @abstract the object supports CalloutOverlayDelegate protocol
 */
@property (assign) id<CalloutOverlayDelegate> calloutDelegate;

/*!
 @property showChevronInCallout
 @abstract YES if chevron should be shown in callouts, NO otherwise
 */
@property (nonatomic, assign) BOOL showChevronInCallout;

/*!
 @property isCalloutFullyClickable [FOR REVIEW]
 @abstract YES if you can touch any place of callout to click chevron, NO otherwise
 */
@property (nonatomic, assign) BOOL isCalloutFullyClickable;

// array of overlay items
/*!
 @property items
 @abstract Array of visual objects displayed on the map
 */
@property (nonatomic, retain) NSMutableArray *items;

/*!
 @property image
 @abstract Image for default object on the map
 */
@property (nonatomic, retain) UIImage *image;

/*!
 @property selectedImage
 @abstract Image for currently selected object on the map
 */
@property (nonatomic, retain) UIImage *selectedImage;


/*!
 @method itemAtIndex:
 @abstract Returns visual object at specified index
 @param index Index of an object
 @result Corresponding OverlayItem object
 */
- (OverlayItem*) itemAtIndex:(NSUInteger)index;

/*!
 @method addItem:
 @abstract Adds the item onto the map
 @param item Visual object
 */
- (void) addItem: (OverlayItem *)item;

/*!
 @method removeItem:
 @abstract Removes the item from the map
 @param item Visual object
 */
- (void) removeItem: (OverlayItem *)item;

/*!
 @method clearItems:
 @abstract Removes all the items from the map
 */
- (void) clearItems;

/*!
 @method hideCallout:
 @abstract Hides the callout
 */
- (void) hideCallout;

/*!
 @method chevronClickedForItem:
 @abstract Chevron button action. Do not use this methods directly, use ProductCalloutOverlayDelegate methods.
 */
- (void) chevronClickedForItem: (OverlayItem *)item;

- (void) showCalloutForItem: (OverlayItem *)item forMapView:(MapView *)mapView withTiledView:(TiledView *)tiledView;
- (void) showCalloutForItem: (OverlayItem *)item forMapView:(MapView *)mapView withTiledView:(TiledView *)tiledView focusCallout:(BOOL)focusCallout;
- (void) focusCallout:(CalloutView *)callout inMapView: (MapView *)mapView withTiledView: (TiledView *)tiledView;

@end
