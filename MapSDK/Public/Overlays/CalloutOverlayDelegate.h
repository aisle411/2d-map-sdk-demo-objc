/***********************************************************************
 *
 * aisle411
 * Copyright (C) by aisle411
 * http://www.aisle411.com
 *
 * Developed by Mercury Development, LLC
 * http://www.mercdev.com
 *
 ***********************************************************************/

#import <UIKit/UIKit.h>

/*!
 @protocol CalloutOverlayDelegate
 @abstract receiver of the callout actions.
 */

@class OverlayItem;

/*!
 @protocol ProductCalloutOverlayDelegate
 @abstract the data source for ProductCalloutOverlay.
 */
@protocol CalloutOverlayDelegate<NSObject>
@optional

/*!
 @method chevronClickedForItem:
 @abstract chevrone button action. Do not use this methods directly, use ProductCalloutOverlayDelegate methods.
 */
- (void) chevronClickedForItem: (OverlayItem *)item;

@end
