![aisle411 Inc.](http://developer.aisle411.com/logo.png)
# README #

_This document is in regards of the aisle411 Inc. Map SDK Demonstration Project._

*****

### What is this repository for? ###

* To Demonstrate Two of the most frequently used Map SDK methods: 
    1. How to Show Map. The SDK should just need the partner information and a store id to show the map.
    2. How to search Term or UPC. The SDK should just need the partner information and a term/UPC to search a product
    3. How do they work together. 
* Version: 1.0
* More information on [**aisle411 Developer Portal**](http://developer.aisle411.com/)

*****

## Important: ##
### How do I get set up? ###

~~~~~
1. Fork the project, clone it into your local repository
2. Open the project in Xcode
3. On the left hand side navigation panel, go to MapSDKDemo -> Supporting Files -> UserInfo.plist
4. Enter your Partner ID and Partner Secret into corresponding values
5. Run the project with internet connection
6. Feel free to go over the code to figure out how are the APIs used
~~~~~

*****

### Contribution guidelines ###

* Feel free to beautify the UI
* Feel free to change code that may improve overall performance
* Send a pull request

### Who do I talk to? ###

* Email: **developers@aisle411.com**
